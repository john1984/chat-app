const createError = require('http-errors');
const express = require('express');
const expHbs = require('express-handlebars');
const path = require('path');
const PORT = process.env.PORT || 3000;
const generateMessage = require('./utils/message');
const checkType = require('./utils/CheckType');
const validateDisplayName = require('./utils/ValidateDisplayName');
const Users = require('./utils/users');
const Rooms = require('./utils/rooms');
const app = express();

const server = require('http').createServer(app);
const io = require('socket.io')(server);

let users = new Users();
let rooms = new Rooms();
// console.log(path.join(__dirname, 'public'));

//configure handlebars as the view engine
app.engine('hbs', expHbs({defaultLayout: 'layout', extname: 'hbs'}));
app.set('view engine', 'hbs');

let duplicateRoomsCounter = 0;

//socket stuff
io.on('connection', (socket) => {
  io.emit('updateActiveRoomsList', rooms.getRooms());

  //listen for join event
  socket.on('join', (params, callback) => {
    const newParams = {
      name: params.name.toLowerCase(),
      room: params.room ? params.room.toLowerCase() : undefined //ternery expression to see if room exists
    };

    if (!checkType(newParams.name) || !checkType(newParams.room)) {
      return callback('Display name and Room are required.');
    }
    socket.join(newParams.room);
    users.removeUser(socket.id);

      //get users list to check if display name is taken
    if (validateDisplayName(newParams.name, users.getUserList(newParams.room))) {
        return callback('Display name already taken.');
    }
    // //add room
    rooms.addRoom(newParams.room);
    // //active rooms event
    io.emit('updateActiveRoomsList', rooms.getRooms());
  
    //add user to room if display name is not taken
    users.addUser(socket.id, newParams.name, newParams.room);
    //emit an updateuserlist event
    io.to(newParams.room).emit('updateUserList', users.getUserList(newParams.room));
    
    //Admin welcome message sent to all sockets (users)
    socket.emit('newMessage', generateMessage('Administrator', 'Welcome to the chat app!'));

    //broadcast a user joined message to all users
    socket.broadcast.to(newParams.room).emit('newMessage', generateMessage('Administrator', `${newParams.name} has joined.`));

    callback();
  });

  socket.on('createMessage', (message, callback) => {
    var user = users.getUser(socket.id);

    if (user && checkType(message.text)) {
      // this emits the message to all users including the one that sent it, which doesn't work for us
      io.to(user.room).emit('newMessage', generateMessage(user.name, message.text));
    }
    callback();
    // //send the message to all connected sockets but the socket that sent it (user)
    // socket.broadcast.emit('newMessage', generateMessage(message.from, message.text));
  });

  //client disconnect
  socket.on('disconnect', () => {
    const user = users.removeUser(socket.id);

    if (user) {
      io.to(user.room).emit('updateUserList', users.getUserList(user.room));
      io.to(user.room).emit('newMessage', generateMessage('Administrator', `${user.name} has left.`));
    }
    //remove room from active room
    if (user) {
      rooms.removeRoom(user.room);
    }
    // setInterval(() => {
      io.emit('updateActiveRoomsList', rooms.getRooms());
    // }, 2000);
  });
});



//app setup

//serve static files
app.use(express.static(path.join(__dirname, '..', 'public')));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    error: err
  });
});

//listening on port 3000
server.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});