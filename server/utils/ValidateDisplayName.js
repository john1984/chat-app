const ValidateDisplayName = (name, usersNameArray) => {
    return usersNameArray.filter(user => user === name)[0];
};

module.exports = ValidateDisplayName;