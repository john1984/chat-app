const socket = io();
const form = document.querySelector('form');
const messageInput = document.getElementById('message');

socket.on('connect', () => {
    const params = {
        name: getQueryVariable('name'),
        room: getQueryVariable('room') || getQueryVariable('selectedRoom'),
    };
    socket.emit('join', params, (error) => {
        if (error) {
            alert(error);
            window.location.href = '/';
        } else {
            console.log('No error!');
        }
    });
});

socket.on('newMessage', (message) => {
    const formattedTime = moment(message.sentAt).format('h:mm a');
    let template = jQuery('#message-template').html();
    let html = Mustache.render(template, {
        text: message.text,
        from: message.from,
        sentAt: formattedTime
    });
    jQuery('#messages').append(html);
    scrollToBottom();

    // let node = document.createElement('li');
    // let textnode = document.createTextNode(`${message.from} ${formattedTime}: ${message.text}`);
    // node.appendChild(textnode);
    // document.getElementById('messages').appendChild(node); 
});

//server disconnect
socket.on('disconnect', () => {
    console.log('Disconnected from server!');
});

socket.on('updateUserList', (users) => {
    var ol = jQuery('<ol></ol>');

    users.forEach(user => {
        ol.append(jQuery('<li></li>').text(user));
    });

    jQuery('#users').html(ol);
});

//message form validation
document.getElementById('message-form').addEventListener('submit', (event) => {
    event.preventDefault();
    const msgInput = document.querySelector('input').value;

    socket.emit('createMessage', {
        text: msgInput
    }, () => {
        form.reset();
    });
});

//autoscroll function

function scrollToBottom() {
    //selectors
    let messages = jQuery('#messages');
    let newMessage = messages.children('li:last-child');
    //heights
    let clientHeight = messages.prop('clientHeight');
    let scrollTop = messages.prop('scrollTop');
    let scrollHeight = messages.prop('scrollHeight');
    let newMessageHeight = newMessage.innerHeight();
    let lastMessageHeight = newMessage.prev().innerHeight();

    if (clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
        messages.scrollTop(scrollHeight);
    }
}

//parse query string function

function getQueryVariable(variable) {
    const query = window.location.search.substring(1);
    const vars = query.split('&');
    for (let i = 0; i < vars.length; i++) {
        let pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) === variable) {
            return decodeURIComponent(pair[1]);
        }
    }
}
